﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ArtUrlWWW_DirLister
{
   public class FsObj
    {
        public long id;
        public string fileName;
        public string dirName;
        public long fileSize;
        public string lastModifiedDate;
        public long isDir;
        public string fullPath;
    }
}
